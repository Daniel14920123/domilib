#ifndef _STRING_H
#define _STRING_H

#include <stdint.h>
#include <stdarg.h>
#include <stddef.h>

/* Conversions */
size_t itoa(int64_t, char *);
size_t utoa(uint64_t, char *);

/* Formatting */
size_t format(char *, size_t, const char *, ...);
size_t vformat(char *, size_t, const char *, va_list);

/* Strings and memory manipulation */
void *mmemcpy(void *, const void *, size_t);
char *mstrncpy(char *, const char *, size_t);
char *mstrcpy(char *, const char *);
void strrev(char *, size_t);

/* Strings examination */
size_t mstrlen (const char *);
int mstrcmp (const char *, const char *);

#endif
