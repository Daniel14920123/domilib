#include <datatypes/string.h>
#include <stdio.h>

/**
 * @brief Return the length of the given byte string
 * @param s The string
 * @return the length of the given string
 */
size_t mstrlen(const char *s)
{
  size_t i = 0;
  while (s[i++])
    ;
  return i - 1;
}
/**
 * @brief Compare two blocks of memory
 * @param lhs The left hand side block
 * @param rhs The right hand side block
 * @param count The block size
 * @return < 0 if lhs < rhs, > 0 is lhs > rhs and 0 if lhs == rhs
 */
int mmemcmp(const void *lhs, const void *rhs, size_t count)
{
  const uint8_t *clhs = (const uint8_t *)lhs;
  const uint8_t *crhs = (const uint8_t *)rhs;
  for (size_t i = 0; i < count; i++)
  {
    uint8_t cl = clhs[i];
    uint8_t cr = crhs[i];
    if (cl != cr)
      return cl - cr;
  }
  return 0;
}
/**
 * @brief Compare two strings
 * @param lhs The left hand side string
 * @param rhs The right hand side string
 * @return < 0 if lhs < rhs, > 0 is lhs > rhs and 0 if lhs == rhs
 */
int mstrcmp (const char *lhs, const char *rhs)
{
  size_t ll = strlen(lhs), rl = strlen(rhs);
  return mmemcmp ((const void *)lhs, (const void *)rhs, ll < rl ? ll : rl);
}

/** 
 * @brief Copy a chunk of memory to another. If the two chunks overlap, the behaviour is undefined
 * @param dest Pointer to the memory location to copy to 
 * @param src Pointer to the memory location to copy from
 * @param count The amount of bytes to be copied
 * @return dest
 */
void *mmemcpy(void *dest, const void *src, size_t count)
{
  uint8_t *cdest = (uint8_t *)dest;
  const uint8_t *csrc  = (const uint8_t *)src;
  for (size_t i = 0; i < count; i++)
    cdest[i] = csrc[i];
  return dest;
}
/** 
 * @brief Copy a chunk of memory to another.
 * @param dest Pointer to the memory location to copy to 
 * @param src Pointer to the memory location to copy from
 * @param count The amount of bytes to be copied
 * @return dest
 */
void *mmemmove(void *dest, const void *src, size_t count)
{
  uint8_t *cdest = (uint8_t *)dest;
  const uint8_t *csrc  = (const uint8_t *)src;

  if (csrc + count >= cdest)
    for (size_t i = count; i > 0; i--)
      cdest[i] = csrc[i];
  else
    mmemcpy (dest, src, count);
  return dest;
}
/**
 * @brief Set a block of memory to a value
 * @param dest The pointer to the memory to fill
 * @param ch The value to set the block to
 * @param count The amount of bytes to set
 * @return dest
 */
void *mmemset(void *dest, int ch, size_t count)
{
  uint8_t *cdest = (uint8_t *)dest;
  for (size_t i = 0; i < count; i++)
    cdest[i] = (uint8_t)ch;
  return dest;
}
/** 
 * @brief Copy count characters from a string to another
 * @param dest pointer to the string to copy to 
 * @param src pointer to the string to copy from
 * @param count the amount of characters to be copied
 * @return dest
 */
char *mstrncpy(char *dest, const char *src, size_t count)
{
  mmemcpy((void *)dest, (const void*)src, count);
  return dest;
}
/** 
 * @brief Copy a string to another
 * @param dest pointer to the string to copy to 
 * @param src pointer to the string to copy from
 * @return dest
 */
char *mstrcpy(char *dest, const char *src)
{
  return mstrncpy(dest, src, mstrlen(src));
}
/**
 * @brief Reverse a string
 * @param s The string to reverse
 * @param count The amount of characters to reverse
 */
void strrev(char *s, size_t count)
{
    for (size_t i = 0; i < count / 2; i++)
    {
        char tmp = s[i];
        size_t r = count - i - 1;
        s[i] = s[r];
        s[r] = tmp;
    }
}
/** 
 * @brief Convert an integer number to a character string
 * @param n The number 
 * @param buffer The string to write the converted number to
 * @param neg The sign indicator.
 */
size_t inttoa(uint64_t n, char *buffer, char neg)
{
  char i = 0;
  n = neg ? n * -1 : n;
  do
  {
    buffer[i] = (char)((n % 10) + '0');
    n /= 10;
    i++;
  } while (n);
  buffer[i + 1] = (char)0;
  strrev (buffer, i);
  if (neg)
  {
      mstrcpy (buffer + 1, buffer);
      buffer[0] = '-';
  }
  return i;
}
/**
 * @brief Convert a 64 bits signed integer to a character string
 * @param n The number
 * @param buffer The string to write the converted number to
 */
size_t mitoa(int64_t n, char *buffer)
{
  char neg = n < 0;
  inttoa ((uint64_t)neg ? -1 * n : n, buffer, neg);
}
/**
 * @brief Convert a 64 bits unsigned integer to a character string
 * @param n The number
 * @param buffer The string to write the converted number to
 */
size_t mutoa(uint64_t n, char *buffer)
{
  inttoa (n, buffer, 0);
}
/**
 * @brief Convert a character string to an unsigned integer
 * @param s The string to convert
 * @return The converted integer or 0 on error
 */
uint64_t matou(const char *s)
{
  int n = 0;
  for (int i = 0; s[i] != 0; i++)
  {
    char cur = s[i];
    if (cur > '9' || cur < '0')
      return 0;
    n = n * 10 + (int)(cur - '0');
  }
  return n;
}
/**
 * @brief Convert a character string to an unsigned integer
 * @param s The string to convert
 * @return The converted integer or 0 on error
 */
int64_t matoi(const char *s)
{
  char mul = 1;
  if (s[0] == '-')
  {
    mul = -1;
    s++; /* Skip the minus */
  }
  return (int)mul * (int)matou(s);
}
typedef enum {
  SignedInt,
  Float,
  UnsignedInt,
  String,
  Str,
  Char,
} FType;

typedef enum {
  Center,
  Left,
  Right,
  None
} AlignType;

typedef struct formatter {
  FType ftype;
  uint8_t precision;
} formatter_t;

int parse_formatter(const char *s, formatter_t *fmt)
{
  if (mstrlen (s) < 1)
    return 1;
  FType ftype; 
  uint8_t precision;
  switch (s[0])
  {
    case 'i':
      ftype = SignedInt;
      break;
    case 'u':
      ftype = UnsignedInt;
      break;
    case 's':
      ftype = Str;
      break;
    case 'S':
      ftype = String;
      break;
    case 'f':
      ftype = Float;
      break;
    case 'c':
      ftype = Char;
      break;
    default:
      return 1;
  }
  
  size_t slen = mstrlen (s);
  for (size_t i = 1; i < slen; i++)
  {
    puts (s);
    char next, n = 0, abuf[4], cur;
    while (next = s[i], next >= '0' && next <= '9' && n < 3)
    {
      abuf[n++] = s[i++];
      if (i + 1 >= slen)
        break;
    }
    uint8_t val = (uint8_t)matou(abuf);
    cur = s[i];
    switch (cur)
    {
      case '.':
        precision = val;
        break;
      default:
        return 1;
    }
  }
  fmt->ftype = ftype;
  fmt->precision = precision;
  return 0;
}

/**
 * @brief Format values inside a string
 * @param buffer The buffer to write the formatted values to
 * @param size The size of the buffer
 * @param format The format to format the values to
 * @param args The values to format
 * @long Each formatter shoukd be enclosed between curly braces (`{ }`) available formatters are:
 * - `s`: `const char *` value
 * - `S`: `struct string` value
 * - `i`: Signed integer value
 * - `u`: Unsigned integer value
 * - `f`: `double` value
 * - `c`: `char` value
 * @return The length of the written string
 */
size_t vformat(char *buffer, size_t size, const char *format, va_list args)
{
  size_t i, length = mstrlen (format), j = 0;
  for (i = 0; i < length; i++)
  {
    if (j >= size)
      return size;
    char cur = format[i];
    if (cur == '{' && cur != '}')
    {
      char fmt[10];
      char next;
      size_t k = 0;
      while (next = format[++i], next != '}' && next != 0)
        fmt[k++] = next;
      fmt[k] = 0;
      formatter_t formatter;
      if (parse_formatter (fmt, &formatter))
        return 0;
      size_t written;
      switch (formatter.ftype)
      {
        case Str:
          {
            const char *s = (const char *)va_arg(args, const char *);
            written = mstrlen (s);
            if (j + written >= size)
              return j;
            mstrcpy (buffer + j, s);
          }
          break;
        case SignedInt:
          {
            char s[20];
            mitoa ((int64_t)va_arg(args, int64_t), s);
            written = mstrlen (s);
            if (j + written >= size)
              return j;
            mstrcpy (buffer + j, s);
          }
          break;
        case Char:
          buffer[j] = (int)va_arg(args, int);
          written = 1;
        case UnsignedInt:
          {
            char s[20];
            mutoa ((uint64_t)va_arg(args, uint64_t), s);
            written = mstrlen (s);
            if (j + written >= size)
              return j;
            mstrcpy (buffer + j, s);
          }
          break;
        case String: case Float: return 0;
        default: return 0;
      }
      j += written;
    }
    else
      buffer[j++] = cur;
  }
  return j;
}
/**
 * @brief Format values inside a string
 * @param buffer The buffer to write the formatted values to
 * @param size The size of the buffer
 * @param format The format to format the values to
 * @param ... The values to format
 * @return The length of the written string
 */
size_t format(char *buffer, size_t size, const char *format, ...)
{
  va_list args;
  va_start (args, format);
  size_t written = vformat(buffer, size, format, args);
  va_end (args);
  return written;
}
