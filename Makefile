CC := gcc
CFLAGS := -Wall -Wextra -O2 -Iinclude/ -std=c11
ALLSRC := $(shell find src/ -name '*.c')
ALLOBJ := $(ALLSRC:.c=.o)

all: domilib.a
domilib.a: $(ALLOBJ)
	ar rcs $@ $^

test: domilib.a
	$(CC) $(CFLAGS) tests.c -L. -l:domilib.a

%.o : %.c
	$(CC) $(CFLAGS) -c -o $@ $<

clean:
	rm -rf $(ALLOBJ) *.a *.elf
