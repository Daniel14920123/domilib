#include <stdio.h>
#include "datatypes/string.h"

#define BUF_SIZE (256)

int main(void) 
{
  char jsp[BUF_SIZE];
  if (!format(jsp, BUF_SIZE, "The answer to {s}, the universe and everything is {i}.", "life", 42))
  {
    puts("Failed to format string.");
    return 1;
  }
  puts (jsp);
  return 0;
}
