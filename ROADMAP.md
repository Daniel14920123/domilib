* [ ] Memory
  * [ ] Real dynamic allocation
  * [ ] Simulated dynamic allocation
* [ ] Datatypes
  * [ ] Tuples
  * [ ] Linked lists
  * [ ] \(Fixed-size)? graph
  * [ ] Map
  * [ ] HashMap
  * [ ] String
    * [ ] String data type (Variable-length character string)
    * [ ] Format functions (format, vformat)
  * [ ] Vector
  * [ ] Fixed-length arrays
* [ ] IO
  * [ ] printf, printfn, print, printn
* [ ] Logging
  * [ ] Level logging (Warn, Error, Info, etc.)
